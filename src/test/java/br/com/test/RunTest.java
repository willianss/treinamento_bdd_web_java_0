package br.com.test;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/main/resources", 
				 tags = "~@ignore", 
				 plugin= {"json:target/cucumber-report.json"},
				 glue = {"br.com"}
				)

public class RunTest {
	
}
